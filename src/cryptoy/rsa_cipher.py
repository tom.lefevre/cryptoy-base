from math import gcd

from cryptoy.utils import (
    draw_random_prime,
    int_to_str,
    modular_inverse,
    pow_mod,
    str_to_int,
)


def keygen() -> dict:
    e = 65537
    # 1. Tire aléatoirement un nombre premier p avec la fonction draw_random_prime
    p = draw_random_prime()
    # 2. Tire aléatoirement un nombre premier q avec la fonction draw_random_prime
    q = draw_random_prime()
    # 3. Calcul de d, l'inverse de e modulo (p - 1) * (q - 1), avec la fonction modular_inverse
    phi = (p - 1) * (q - 1)
    d = modular_inverse(e, phi)
    # 4. Renvoie un dictionnaire { "public_key": (e, p * q), "private_key": d}
    return {"public_key": (e, p * q), "private_key": d}


def encrypt(msg: str, public_key: tuple) -> int:
    # 1. Convertir le message en nombre entier avec la fonction str_to_int
    m = str_to_int(msg)
    # 2. Vérifiez que ce nombre est < public_key[1], sinon lancer une exception
    if m >= public_key[1]:
        raise ValueError("Message too large for encryption.")
    # 3. Chiffrez le nombre entier avec pow_mod et les paramètres de la clef publique (e, N)
    return pow_mod(m, public_key[0], public_key[1])


def decrypt(msg: int, key: dict) -> str:
    # 1. Utilisez pow_mod avec les paramètres de la clé
    m = pow_mod(msg, key["private_key"], key["public_key"][1])
    # 2. Convertir l'entier calculé en str avec la fonction int_to_str
    return int_to_str(m)
