from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César
def encrypt(msg: str, shift: int) -> str:
    unicodes = str_to_unicodes(msg)
    encrypted_unicodes = [(x + shift) % 0x110000 for x in unicodes]
    return unicodes_to_str(encrypted_unicodes)

def decrypt(msg: str, shift: int) -> str:
    return encrypt(msg, -shift)

def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    # Brute-force attack: Try all possible shift values (0 to 65535) to find the correct key
    for shift in range(0x110000):
        decrypted_msg = decrypt(s, shift)
        if "ennemis" in decrypted_msg:
            return decrypted_msg, shift

    # If the loop completes without finding the correct key, raise an exception
    raise RuntimeError("Failed to attack")
