from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    result = [(a * i + b) % n for i in range(n)]
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    result = [perm.index(i) for i in range(n)]
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    n = 1114112  # Number of Unicode characters (maximum possible value)
    uni_msg = str_to_unicodes(msg)
    perm = compute_permutation(a, b, n)
    encrypted_uni_msg = [(perm[uni] if uni < n else uni) for uni in uni_msg]
    encrypted_msg = unicodes_to_str(encrypted_uni_msg)
    return encrypted_msg


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    n = 1114112  # Number of Unicode characters (maximum possible value)
    uni_msg = str_to_unicodes(msg)
    encrypted_uni_msg = [(a * uni + b) % n if uni < n else uni for uni in uni_msg]
    encrypted_msg = unicodes_to_str(encrypted_uni_msg)
    return encrypted_msg


def decrypt(msg: str, a: int, b: int) -> str:
    n = 1114112  # Number of Unicode characters (maximum possible value)
    uni_msg = str_to_unicodes(msg)
    inv_perm = compute_inverse_permutation(a, b, n)
    decrypted_uni_msg = [(inv_perm[uni] if uni < n else uni) for uni in uni_msg]
    decrypted_msg = unicodes_to_str(decrypted_uni_msg)
    return decrypted_msg

def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    n = 1114112  # Number of Unicode characters (maximum possible value)
    uni_msg = str_to_unicodes(msg)
    decrypted_uni_msg = [((a_inverse * (uni - b)) % n if uni < n else uni) for uni in uni_msg]
    decrypted_msg = unicodes_to_str(decrypted_uni_msg)
    return decrypted_msg


def compute_affine_keys(n: int) -> list[int]:
    return [a for a in range(1, n) if gcd(a, n) == 1]


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for a_inv in affine_keys:
        if a * a_inv % n == 1:
            return a_inv
    raise RuntimeError(f"{a} has no inverse")



def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    info_substring = "bombe"
    target_b = 58

    n = 1114112  # Number of Unicode characters (maximum possible value)
    affine_keys = compute_affine_keys(n)

    for a in affine_keys:
        try:
            b = compute_affine_key_inverse(a, affine_keys, n)
            decrypted_msg = decrypt(s, a, b)
            if info_substring in decrypted_msg and b == target_b:
                return decrypted_msg, (a, b)
        except RuntimeError:
            continue

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = ("જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51")
    info_substring = "bombe"

    n = 1114112  # Number of Unicode characters (maximum possible value)
    affine_keys = compute_affine_keys(n)

    for a in affine_keys:
        try:
            b = compute_affine_key_inverse(a, affine_keys, n)
            decrypted_msg = decrypt_optimized(s, a, b)
            if info_substring in decrypted_msg:
                return decrypted_msg, (a, b)
        except RuntimeError:
            continue

    raise RuntimeError("Failed to attack")
