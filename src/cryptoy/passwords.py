import hashlib
import os
from random import Random
import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(passwords: list[str], count: int = 32) -> dict[str, str]:
    rng = Random()

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    # Implémentation de l'attaque par dictionnaire
    for user, password_hash in passwords_database.items():
        for password in passwords:
            if hash_password(password) == password_hash:
                users_and_passwords[user] = password
                break

    return users_and_passwords


def fix(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, dict[str, str]]:
    users_and_salt = {}
    new_database = {}

    for user, password_hash in passwords_database.items():
        salt = random_salt()
        password = None
        # On ne stocke pas le mot de passe en clair, seulement le hash et le sel
        users_and_salt[user] = {"password_hash": password_hash, "password_salt": salt}

    for user, data in users_and_salt.items():
        stored_salt = data["password_salt"]
        stored_hashed_password = data["password_hash"]
        for password in passwords:
            computed_hashed_password = hash_password(stored_salt + password)
            if stored_hashed_password == computed_hashed_password:
                new_database[user] = data
                break

    return new_database


def authenticate(user: str, password: str, new_database: dict[str, dict[str, str]]) -> bool:
    if user not in new_database:
        return False

    stored_salt = new_database[user]["password_salt"]
    stored_hashed_password = new_database[user]["password_hash"]
    computed_hashed_password = hash_password(stored_salt + password)

    return stored_hashed_password == computed_hashed_password
