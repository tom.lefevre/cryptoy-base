import random
import sys
from cryptoy.utils import pow_mod

sys.setrecursionlimit(5000)  # Required for pow_mod for large exponents


def keygen(prime_number: int, generator: int) -> dict[str, int]:
    # 1. Generate a random secret private_key between 2 and prime_number - 1 (inclusive)
    private_key = random.randint(2, prime_number - 1)

    # 2. Calculate the public key using the formula: public_key = generator ** private_key % prime_number
    public_key = pow_mod(generator, private_key, prime_number)

    # 3. Return the dictionary containing the public_key and private_key
    return {"public_key": public_key, "private_key": private_key}


def compute_shared_secret_key(public: int, private: int, prime_number: int) -> int:
    # Calculate the shared secret key using the formula: shared_secret_key = public ** private % prime_number
    shared_secret_key = pow_mod(public, private, prime_number)
    return shared_secret_key
