from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    backend = default_backend()
    aesgcm = Cipher(algorithms.AES(key), modes.GCM(nonce), backend=backend).encryptor()
    ciphertext = aesgcm.update(msg) + aesgcm.finalize()
    return ciphertext

def decrypt(ciphertext: bytes, key: bytes, nonce: bytes) -> bytes:
    backend = default_backend()
    aesgcm = Cipher(algorithms.AES(key), modes.GCM(nonce), backend=backend).decryptor()
    decrypted_msg = aesgcm.update(ciphertext) + aesgcm.finalize()
    return decrypted_msg
